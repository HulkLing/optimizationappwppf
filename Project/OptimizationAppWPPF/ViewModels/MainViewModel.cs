﻿using System.Globalization;
using OdeSolver.BLL.Logics;
using OptimizationAppWPPF.Commands;
using OptimizationAppWPPF.Models;

namespace OptimizationAppWPPF.ViewModels
{
    internal class MainViewModel
    {
        private RelayCommand _solverCommand;

        public MainViewModel()
        {
            LabelModel = new LabelModel();
            TextboxModel = new TextboxModel();
            TextblockModel = new TextblockModel();
        }

        public LabelModel LabelModel { get; }
        public TextboxModel TextboxModel { get; set; }
        public TextblockModel TextblockModel { get; set; }

        public RelayCommand SolverCommand
        {
            get
            {
                return _solverCommand ??
                       (_solverCommand = new RelayCommand(obj =>
                       {
                           var aVd = new AlternatingVariableDescent();
                           var result = aVd.Run(TextboxModel.ArrayOfTextboxes);
                           TextblockModel.Textblock += string.IsNullOrEmpty(result.Item3)
                               ? $"{result.Item1} ( {result.Item2[0].ToString(CultureInfo.InvariantCulture).Replace(",", ".")}," +
                                 $"{result.Item2[1].ToString(CultureInfo.InvariantCulture).Replace(",", ".")}," +
                                 $"{result.Item2[2].ToString(CultureInfo.InvariantCulture).Replace(",", ".")}," +
                                 $"{result.Item2[3].ToString(CultureInfo.InvariantCulture).Replace(",", ".")} )\n"
                               : result.Item3 + "\n";
                       }));
            }
        }
    }
}