﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using OptimizationAppWPPF.Annotations;

namespace OptimizationAppWPPF.Models
{
    internal class TextblockModel : INotifyPropertyChanged
    {
        private string _textblock;

        public string Textblock
        {
            get => _textblock;
            set
            {
                if (_textblock == value)
                    return;

                _textblock = value;
                OnPropertyChanged(nameof(Textblock));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}