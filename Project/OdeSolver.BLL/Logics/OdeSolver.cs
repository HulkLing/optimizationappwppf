﻿using System;
using System.Net;
using MathWorks.MATLAB.ProductionServer.Client;
using OdeSolver.BLL.Constants;
using OdeSolver.BLL.Interfaces;

namespace OdeSolver.BLL.Logics
{
    public class OdeSolver
    {
        private readonly MWClient _client;

        public OdeSolver()
        {
            _client = new MWHttpClient();
        }

        public Tuple<IOdeSolver, string> CreateClient()
        {
            var message = string.Empty;
            try
            {
                var oSol = _client.CreateProxy<IOdeSolver>(new Uri(ConnectionStrings.OdeSolverConnectionString));
                return new Tuple<IOdeSolver, string>(oSol, message);
            }

            catch (Exception e)
            {
                message = e.Message;
                return new Tuple<IOdeSolver, string>(null, message);
            }
        }

        public Tuple<double, string> Solve(IOdeSolver iOdeSolver, double[] i)
        {
            double[] t = {0, 20};
            double[] x = {2.0, 3.0, 1.5, 10.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
            double[,] arraySolver;
            var message = string.Empty;
            try
            {
                arraySolver = iOdeSolver.OdeSolver(t, x, i);
            }
            catch (MATLABException ex)
            {
                message = @"MATLAB error thrown : 
                                    " + ex.MATLABIdentifier + @"
                                        " + ex.MATLABStackTraceString;
                return new Tuple<double, string>(default(double), message);
            }
            catch (WebException ex)
            {
                var response = (HttpWebResponse) ex.Response;
                if (response != null)
                {
                    message = @"Status code : " + response.StatusCode + @"
                                        " + @"Status description : " + response.StatusDescription;
                    return new Tuple<double, string>(default(double), message);
                }
                message = @"No response received in WebException with status : " + ex.Status;
                return new Tuple<double, string>(default(double), message);
            }
            catch (Exception e)
            {
                message = e.Message;
                return new Tuple<double, string>(default(double), message);
            }

            var lastElementIndex = arraySolver.GetLength(1) - 1;
            var result = 0.0;
            for (var j = 0; j < 5; j++)
                result += arraySolver[j, lastElementIndex];
            return new Tuple<double, string>(result, message);
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}