﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using OptimizationAppWPPF.Annotations;

namespace OptimizationAppWPPF.Models
{
    internal class LabelModel : INotifyPropertyChanged
    {
        private string _label1 = "Iα:";
        private string _label2 = "Iz:";
        private string _label3 = "Iv:";
        private string _label4 = "Ih:";
        private string _label5 = "Результат";

        public string Label1
        {
            get => _label1;
            set
            {
                if (_label1 == value)
                    return;

                _label1 = value;
                OnPropertyChanged(nameof(Label1));
            }
        }

        public string Label2
        {
            get => _label2;
            set
            {
                if (_label2 == value)
                    return;

                _label2 = value;
                OnPropertyChanged(nameof(Label2));
            }
        }

        public string Label3
        {
            get => _label3;
            set
            {
                if (_label3 == value)
                    return;

                _label3 = value;
                OnPropertyChanged(nameof(Label3));
            }
        }

        public string Label4
        {
            get => _label4;
            set
            {
                if (_label4 == value)
                    return;

                _label4 = value;
                OnPropertyChanged(nameof(Label4));
            }
        }
        public string Label5
        {
            get => _label5;
            set
            {
                if (_label5 == value)
                    return;

                _label5 = value;
                OnPropertyChanged(nameof(Label5));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}