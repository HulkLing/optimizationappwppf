﻿namespace OdeSolver.BLL.Interfaces
{
    public interface IOdeSolver
    {
        double[,] OdeSolver(double[] t, double[] x, double[] i);
    }
}