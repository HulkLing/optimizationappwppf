﻿using System;

namespace OdeSolver.BLL.Logics
{
    public class AlternatingVariableDescent
    {
        public Tuple<double, double[], string> Run(double[] arrayDoubles)
        {
            var solver = new OdeSolver();
            var clientResult = solver.CreateClient();

            if (string.IsNullOrEmpty(clientResult.Item2))
            {
                var result = solver.Solve(clientResult.Item1, arrayDoubles);
                return new Tuple<double, double[], string>(result.Item1, arrayDoubles, result.Item2);
            }
            solver.Dispose();
            return new Tuple<double, double[], string>(default(double), default(double[]), clientResult.Item2);
        }
    }
}