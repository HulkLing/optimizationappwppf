﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using OptimizationAppWPPF.Annotations;

namespace OptimizationAppWPPF.Models
{
    internal class TextboxModel : INotifyPropertyChanged
    {
        private string _textbox1 = "6,12";
        private string _textbox2 = "4,32";
        private string _textbox3 = "6,82";
        private string _textbox4 = "0,359";

        public double[] ArrayOfTextboxes
        {
            get
            {
                var separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
                
                return new[]
                {
                    Convert.ToDouble(Textbox1.Replace('.', separator).Replace(',', separator)),
                    Convert.ToDouble(Textbox2.Replace('.', separator).Replace(',', separator)),
                    Convert.ToDouble(Textbox3.Replace('.', separator).Replace(',', separator)),
                    Convert.ToDouble(Textbox4.Replace('.', separator).Replace(',', separator))
                };
            }
        }

        public string Textbox1
        {
            get => _textbox1;
            set
            {
                if (_textbox1 == value)
                    return;

                _textbox1 = value;
                OnPropertyChanged(nameof(Textbox1));
            }
        }

        public string Textbox2
        {
            get => _textbox2;
            set
            {
                if (_textbox2 == value)
                    return;

                _textbox2 = value;
                OnPropertyChanged(nameof(Textbox2));
            }
        }

        public string Textbox3
        {
            get => _textbox3;
            set
            {
                if (_textbox3 == value)
                    return;

                _textbox3 = value;
                OnPropertyChanged(nameof(Textbox3));
            }
        }

        public string Textbox4
        {
            get => _textbox4;
            set
            {
                if (_textbox4 == value)
                    return;

                _textbox4 = value;
                OnPropertyChanged(nameof(Textbox4));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}